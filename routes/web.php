<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Route::get('/', function(){
    return view('auth.login');
});



Auth::routes();

Route::middleware(['auth:web'])->group(function(){
    Route::get('dashboard', 'DashboardController@index');
    Route::resource('unit-organisasi', 'UnitOrganisasiController');
    Route::resource('jabatan', 'JabatanController');
    Route::resource('pegawai', 'PegawaiController');
    Route::resource('golongan', 'GolonganController');
    Route::resource('user', 'UserController');
    Route::resource('mutasi', 'MutasiController');
    Route::get('pensiun', 'PensiunController@index')->name('pensiun.index');
    Route::get('pensiun/create', 'PensiunController@create')->name('pensiun.create');
    Route::post('pensiun/store', 'PensiunController@store')->name('pensiun.store');
    Route::get('pensiun/{id}', 'PensiunController@edit')->name('pensiun.edit');
    Route::put('pensiun/{id}', 'PensiunController@update')->name('pensiun.update');
    Route::delete('pensiun/{id}', 'PensiunController@destroy')->name('pensiun.destroy');
    Route::get('info-unit-organisasi', 'PegawaiController@eksePegawai')->name('ekse-pegawai');

    Route::get('print-pegawai-organisasi/{id}', 'PrintController@printPegawaiOrganisasi')->name('print.pegawaiOrganisasi');
    Route::get('print-pegawai/{id}', 'PrintController@printPegawai')->name('print.pegawai');
    Route::get('print-mutasi/{id}', 'PrintController@printMutasi')->name('print.mutasi');
    Route::get('print-surat-penugasan/{id}', 'PrintController@printSuratPenugasan')->name('print.suratPenugasan');
});
