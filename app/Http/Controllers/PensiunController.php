<?php

namespace App\Http\Controllers;

use App\Golongan;
use App\Jabatan;
use App\JabatanPegawai;
use App\Pegawai;
use App\UnitOrganisasi;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PensiunController extends Controller
{
    public function __construct()
    {
        $this->middleware('read-only', ['except' => ['index']]);
    }
    
    public function index()
    {
        $org = UnitOrganisasi::where('nama_unit_organisasi', 'Pensiun')->first();
        $pensiun = JabatanPegawai::where('unit_organisasi_id', $org->id)->get();
        
        return view('pensiun.index', ['pensiun' => $pensiun]);
    }

    public function create()
    {
        $org = UnitOrganisasi::where('nama_unit_organisasi', 'Pensiun')->first();
        $jab = Jabatan::where('nama_jabatan', 'Pensiun')->first();
        $pensiun_ids = JabatanPegawai::where('unit_organisasi_id', $org->id)->get()->pluck('pegawai_id')->all();
        
        return view('pensiun.form', [
            'unit_organisasi_id' => $org->id,
            'jabatan_id' => $jab->id,
            'pensiun' => JabatanPegawai::getDefaultValues(),
            'pegawai' => Pegawai::orderBy('nama', 'desc')->get(),
            'pensiun_ids' => $pensiun_ids
        ]);
    }

    public function store(Request $request)
    {
        $input = $request->toArray();
        $pegawai = Pegawai::findOrFail($request->pegawai_id);
        $age = Carbon::parse($pegawai->tanggal_lahir)->age;
        $input['tgl_sk'] = $input['tmt_jabatan'];
        
        if($age <= 58) {
            return redirect()->route('pensiun.index')->with('error', 'Usia minimal 58 tahun');
        }

        JabatanPegawai::create($input);
        JabatanPegawai::updateJabatanSekarang($request->pegawai_id);
        return redirect()->route('pensiun.index')->with('success', 'Berhasil menambah data pensiun');
    }

    public function edit($id)
    {
        $org = UnitOrganisasi::where('nama_unit_organisasi', 'Pensiun')->first();
        $jab = Jabatan::where('nama_jabatan', 'Pensiun')->first();
        return view('pensiun.form', [
            'unit_organisasi_id' => $org->id,
            'jabatan_id' => $jab->id,
            'pensiun' => JabatanPegawai::find($id),
            'pegawai' => Pegawai::orderBy('nama', 'desc')->get(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $input = $request->toArray();
        $input['tgl_sk'] = $input['tmt_jabatan'];
        JabatanPegawai::find($id)->update($input);
        JabatanPegawai::updateJabatanSekarang($request->pegawai_id);
        return redirect()->route('pensiun.index')->with('success', 'Berhasil menambah data pensiun');
    }

    public function destroy($id)
    {
        JabatanPegawai::find($id)->delete();
        return redirect()->route('pensiun.index')->with('success', 'Berhasil menghapus data pensiun');
    }
}
