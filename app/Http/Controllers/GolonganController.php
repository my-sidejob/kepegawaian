<?php

namespace App\Http\Controllers;

use App\Golongan;
use App\Pegawai;
use Illuminate\Http\Request;

class GolonganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('read-only', ['except' => ['index']]);
    }

    public function index()
    {
        $data['golongan'] = Golongan::all();
        return view("golongan.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['golongan'] = Golongan::getDefaultValues();
        $data['pegawai'] = Pegawai::orderBy('nama')->get();
        return view("golongan.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pegawai_id' => 'required',
            'golongan' => 'required',
            'no_sk' => 'required',
            'tanggal_sk' => 'required',
            'tmt_golongan' => 'required',
        ]);

        Golongan::create($request->toArray());
        return redirect()->route('golongan.index')->with('success', 'Berhasil menambah golongan pegawai');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pegawai'] = Pegawai::orderBy('nama')->get();
        $data['golongan'] = Golongan::find($id);
        return view("golongan.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pegawai_id' => 'required',
            'golongan' => 'required',
            'no_sk' => 'required',
            'tanggal_sk' => 'required',
            'tmt_golongan' => 'required',
        ]);

        Golongan::find($id)->update($request->toArray());
        return redirect()->route('golongan.index')->with('success', 'Berhasil mengubah golongan pegawai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Golongan::find($id)->delete();
        return redirect()->route('golongan.index')->with('success', 'Berhasil menghapus golongan pegawai');
    }
}
