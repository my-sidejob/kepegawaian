<?php

namespace App\Http\Controllers;

use App\Golongan;
use App\JabatanPegawai;
use App\Pegawai;
use App\UnitOrganisasi;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class PrintController extends Controller
{
    public function printPegawaiOrganisasi($unit_organisasi_id)
    {
        $data['unit_organisasi'] = UnitOrganisasi::findOrFail($unit_organisasi_id);
        $data['pegawai'] = JabatanPegawai::where("jabatan_sekarang", 'true')->where('unit_organisasi_id', $unit_organisasi_id)->get();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('print.pegawai_unit_organisasi', $data);
        return $pdf->stream();
    }

    public function printPegawai($id)
    {
        $data['pegawai'] = Pegawai::findOrFail($id);
        $data['jabatan_sekarang'] = JabatanPegawai::where("jabatan_sekarang", 'true')->where('pegawai_id', $id)->first();
        
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('print.pegawai', $data);
        return $pdf->stream();

        //return view('print.pegawai', $data);
    }

    public function printMutasi($id)
    {
        $data['pegawai'] = Pegawai::findOrFail($id);
        $data['jabatan_pegawai'] = JabatanPegawai::where('pegawai_id', $id)->orderBy('tmt_jabatan', 'desc')->get();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('print.mutasi', $data);
        return $pdf->stream();
    }

    public function printSuratPenugasan($id)
    {
        $data['jabatan_pegawai'] = JabatanPegawai::find($id);
        $data['golongan'] = Golongan::where('pegawai_id', $data['jabatan_pegawai']->pegawai_id)->first();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('print.surat_penugasan', $data);
        return $pdf->stream();
        //return view('print.surat_penugasan', $data);
      
    }
}
