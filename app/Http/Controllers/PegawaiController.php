<?php

namespace App\Http\Controllers;

use App\Golongan;
use App\JabatanPegawai;
use App\Pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('read-only', ['except' => ['index', 'eksePegawai']]);
    }

    public function index()
    {
        $data['pegawai'] = Pegawai::all();
        return view("pegawai.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['pegawai'] = Pegawai::getDefaultValues();
        return view("pegawai.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required', //'Laki - laki', 'Perempuan',
            'alamat' => 'required',
            'no_telp' => 'required',
            'email' => 'required',
            'nik' => 'required|unique:pegawai',
            'jenis_pegawai' => 'required',
        ]);

        if($request->jenis_pegawai == 2) {
            $request->validate([
                'no_sk_kepala_dinas' => 'required'
            ]);
        } else {
            $request->validate([
                'nip' => 'required|unique:pegawai|digits:18', //(unique)
                'tmt_pns' => 'required',
                'tmt_cpns' => 'required',
            ]);
        }

        Pegawai::create($request->toArray());
        return redirect()->route('pegawai.index')->with('success', 'Berhasil menambah data pegawai');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pegawai'] = Pegawai::find($id);
        return view("pegawai.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required', //'Laki - laki', 'Perempuan',
            'alamat' => 'required',
            'no_telp' => 'required',
            'email' => 'required',
            'nik' => 'required|unique:pegawai,id,'.$id,
            'jenis_pegawai' => 'required',
        ]);

        if($request->jenis_pegawai == 2) {
            $request->validate([
                'no_sk_kepala_dinas' => 'required'
            ]);
        } else {
            $request->validate([
                'nip' => 'required|digits:18|unique:pegawai,id,'.$id, //(unique)
                'tmt_pns' => 'required',
                'tmt_cpns' => 'required',
            ]);
        }

        Pegawai::find($id)->update($request->toArray());
        return redirect()->route('pegawai.index')->with('success', 'Berhasil mengubah pegawai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = Pegawai::find($id);
        $golongan = Golongan::where('pegawai_id', $id)->first();
        if($golongan != null) {
            $golongan->delete();
        }
        $jabatan_pegawai = JabatanPegawai::where('pegawai_id', $id)->delete();
        $pegawai->delete();
        return redirect()->route('pegawai.index')->with('success', 'Berhasil menghapus unit organisasi');
    }

    public function eksePegawai()
    {

        if(Auth::user()->level == 'Kepala Sub-bagian') {
            $jabatan_pegawai = JabatanPegawai::selectRaw('unit_organisasi_id, count(pegawai_id) as jumlah_sekarang')->where('jabatan_sekarang', 'true')->groupBy('unit_organisasi_id')->get();
            $data['jabatan_pegawai'] = $jabatan_pegawai;
            return view('pegawai.ekse_pegawai', $data);
        } else {
            return "Unauthorized";
        }
    }
}
