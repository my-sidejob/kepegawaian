<?php

namespace App\Http\Controllers;

use App\JabatanPegawai;
use App\Pegawai;
use App\UnitOrganisasi;
use Illuminate\Http\Request;

class UnitOrganisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('read-only', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $data['unit_organisasi'] = UnitOrganisasi::all();
        return view("unit_organisasi.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['unit_organisasi'] = UnitOrganisasi::getDefaultValues();
        return view("unit_organisasi.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_unit_organisasi' => 'required'
        ]);

        UnitOrganisasi::create($request->toArray());
        return redirect()->route('unit-organisasi.index')->with('success', 'Berhasil menambah unit organisasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['unit_organisasi'] = UnitOrganisasi::findOrFail($id);
        $data['pegawai'] = JabatanPegawai::where("jabatan_sekarang", 'true')->where('unit_organisasi_id', $id)->get();
        return view('unit_organisasi.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['unit_organisasi'] = UnitOrganisasi::find($id);
        return view("unit_organisasi.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_unit_organisasi' => 'required'
        ]);

        UnitOrganisasi::find($id)->update($request->toArray());
        return redirect()->route('unit-organisasi.index')->with('success', 'Berhasil mengubah unit organisasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit_org = UnitOrganisasi::find($id);

        $jabatan_peg = JabatanPegawai::where('unit_organisasi_id', $id)->get();

        if($jabatan_peg->count() == 0) {
            $unit_org->delete();
        } else {
            return redirect()->route('unit-organisasi.index')->with('error', 'Gagal menghapus unit organisasi karena record tersebut memiliki pegawai.');
        }

        return redirect()->route('unit-organisasi.index')->with('success', 'Berhasil menghapus unit organisasi');
    }
}
