<?php

namespace App\Http\Controllers;

use App\Jabatan;
use App\JabatanPegawai;
use Illuminate\Http\Request;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('read-only', ['except' => ['index']]);
    }


    public function index()
    {
        $data['jabatan'] = Jabatan::all();
        return view("jabatan.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['jabatan'] = Jabatan::getDefaultValues();
        return view("jabatan.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_jabatan' => 'required'
        ]);

        Jabatan::create($request->toArray());
        return redirect()->route('jabatan.index')->with('success', 'Berhasil menambah jabatan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['jabatan'] = Jabatan::find($id);
        return view("jabatan.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_jabatan' => 'required'
        ]);

        Jabatan::find($id)->update($request->toArray());
        return redirect()->route('jabatan.index')->with('success', 'Berhasil mengubah jabatan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jabatan = Jabatan::find($id);
        $jabatan_peg = JabatanPegawai::where('unit_organisasi_id', $id)->get();
       
        if($jabatan_peg->count() == 0) {
            $jabatan->delete();
        } else {
            return redirect()->route('jabatan.index')->with('error', 'Gagal menghapus jabatan karena record tersebut memiliki pegawai.');
        }
        return redirect()->route('jabatan.index')->with('success', 'Berhasil menghapus jabatan');
    }
}
