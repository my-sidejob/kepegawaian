<?php

namespace App\Http\Controllers;

use App\Jabatan;
use App\JabatanPegawai;
use App\Pegawai;
use App\UnitOrganisasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MutasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    
    public function index(Request $request)
    {
        $data['pegawai'] = Pegawai::orderBy('nama', 'asc')->get();
        $data['jabatan_pegawai'] = null;
        if(isset($request->pegawai_id)) {
            $data['pegawai_single'] = Pegawai::findOrFail($request->pegawai_id);
            $data['jabatan_pegawai'] = JabatanPegawai::where('pegawai_id', $request->pegawai_id)->orderBy('tmt_jabatan', 'desc')->get();
        }

        return view('mutasi.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->level == 'Kepala Sub-bagian') {
        
            $data['mutasi'] = JabatanPegawai::getDefaultValues();
            $data['pegawai'] = Pegawai::orderBy('nama', 'asc')->get();
            $data['unit_organisasi'] = UnitOrganisasi::orderBy('nama_unit_organisasi', 'asc')->where('nama_unit_organisasi', '!=', 'Pensiun')->get();
            $data['jabatan'] = Jabatan::orderBy('nama_jabatan', 'asc')->where('nama_jabatan', '!=', 'Pensiun')->get();
            return view('mutasi.form', $data);
        } else {
            return "Unauthorized";
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->level == 'Kepala Sub-bagian') {
            $request->validate([
                'pegawai_id' => 'required',
                'unit_organisasi_id' => 'required',
                'jabatan_id' => 'required',
                'tmt_jabatan' => 'required',
                'tgl_sk' => 'required',
                'no_sk' => 'required',
                
            ]);

            JabatanPegawai::create($request->toArray());
            JabatanPegawai::updateJabatanSekarang($request->pegawai_id);
            return redirect('mutasi?pegawai_id='.$request->pegawai_id)->with('success', 'Berhasil menambahkan mutasi pegawai');
        } else {
            return "Unauthorized";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->level == 'Kepala Sub-bagian') {
            $data['mutasi'] = JabatanPegawai::findOrFail($id);
            $data['pegawai'] = Pegawai::orderBy('nama', 'asc')->get();
            $data['unit_organisasi'] = UnitOrganisasi::orderBy('nama_unit_organisasi', 'asc')->get();
            $data['jabatan'] = Jabatan::orderBy('nama_jabatan', 'asc')->get();
            return view('mutasi.form', $data);
        } else {
            return "Unauthorized";
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->level == 'Kepala Sub-bagian') {
            $request->validate([
                'pegawai_id' => 'required',
                'unit_organisasi_id' => 'required',
                'jabatan_id' => 'required',
                'tmt_jabatan' => 'required',
                'tgl_sk' => 'required',
                'no_sk' => 'required',
                
            ]);

            JabatanPegawai::find($id)->update($request->toArray());
            JabatanPegawai::updateJabatanSekarang($id);
            return redirect('mutasi?pegawai_id='.$request->pegawai_id)->with('success', 'Berhasil mengubah mutasi pegawai');
        } else {
            return "Unauthorized";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->level == 'Kepala Sub-bagian') {
            JabatanPegawai::find($id)->delete();
            return redirect('mutasi?pegawai_id='.$_GET['pegawai_id'])->with('success', 'Berhasil menghapus mutasi pegawai');
        } else {
            return "Unauthorized";
        }

    }
}
