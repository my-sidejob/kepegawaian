<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ReadOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = Auth::user()->level;
        if($auth != 'Kepala Sub-bagian') {
            return $next($request);
        } else {
            return response('Unauthorized.', 401);
        }
    }
}
