<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Orangtua;

class StoreOrangtua extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $orangtua = Orangtua::find($this->orangtua);
                
        switch($this->method()){
            case 'POST': 
            {
                return [
                    'username' => 'required|unique:orangtua,username',                    
                    'email' => 'required|unique:orangtua,email',
                    'alamat' => 'required',
                    'no_telp' => 'required|numeric',
                    'nama_orangtua' => 'required',
                    'password' => 'required',                                      
                ];
            }
            case 'PUT':
            case 'PATCH':
            {                                
                return [
                    'username' => 'required|unique:orangtua,username,' . $orangtua->id,                    
                    'email' => 'required|unique:orangtua,email,' . $orangtua->id,
                    'alamat' => 'required',
                    'no_telp' => 'required|numeric',
                    'nama_orangtua' => 'required',                
                ];
            }
        }
    }
}
