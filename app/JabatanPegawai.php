<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JabatanPegawai extends Model
{
    protected $table = 'jabatan_pegawai';
    protected $fillable = [
        'pegawai_id',
        'unit_organisasi_id',
        'jabatan_id',
        'tmt_jabatan',
        'tgl_sk',
        'no_sk',
        'jabatan_sekarang',
        'eselon',
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'pegawai_id' => '',
            'unit_organisasi_id' => '',
            'jabatan_id' => '',
            'tmt_jabatan' => '',
            'tgl_sk' => '',
            'no_sk' => '',
            'eselon' => '',
        ];
    }

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai');
    }

    public function unitOrganisasi()
    {
        return $this->belongsTo('App\UnitOrganisasi');
    }

    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan');
    }

    public static function updateJabatanSekarang($pegawai_id)
    {
        $jabatan = JabatanPegawai::where('pegawai_id', $pegawai_id)->orderBy('tmt_jabatan', 'desc')->get();

        foreach($jabatan as $key => $record) {
            if($key == 0) {
                $value = 'true';
            } else {
                $value = 'false';
            }

            $record->update([
                'jabatan_sekarang' => $value
            ]);
        }
    }

    public static function eselonList()
    {
        return [
            'II B', 'III A', 'III B', 'IV A', 'IV B', 'V A'
        ];
    }

}
