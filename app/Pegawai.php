<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $fillable = [
        'nama',
        'gelar_depan',
        'gelar_belakang',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin', //'Laki - laki', 'Perempuan',
        'alamat',
        'no_telp',
        'email',
        'nik',
        'nip', //(unique)
        'tmt_pns',
        'tmt_cpns',
        'jenis_pegawai',
        'no_sk_kepala_dinas',
        'no_urut_umum',
        'sk_pns',
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'nama' => '',
            'gelar_depan' => '',
            'gelar_belakang' => '',
            'tempat_lahir' => '',
            'tanggal_lahir' => '',
            'jenis_kelamin' => '', //'Laki - laki', 'Perempuan',
            'alamat' => '',
            'no_telp' => '',
            'email' => '',
            'nik' => '',
            'nip' => '', //(unique)
            'tmt_pns' => '',
            'tmt_cpns' => '',
            'jenis_pegawai' => '',
            'no_sk_kepala_dinas' => '',
            'no_urut_umum' => '',
            'sk_pns' => ''
        ];
    }


    public function namaGelar()
    {
        if($this->gelar_depan != null) {
            $gelar_1 = $this->gelar_depan . ' ';
        } else {
            $gelar_1 = '';
        }

        if($this->gelar_belakang != null) {
            $gelar_2 = $this->gelar_belakang . ' ';
        } else {
            $gelar_2 = '';
        }

        return $gelar_1.$this->nama. ' ' .$gelar_2;
    }
    public function jenisPegawai()
    {
        if($this->jenis_pegawai == 1) {
            return 'PNS';
        } else {
            return 'Non-PNS / Kontrak / Lainnya';
        }
    }
}
