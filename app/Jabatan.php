<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'jabatan';
    protected $fillable = [
        'nama_jabatan',
        'keterangan'
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'nama_jabatan' => '',
            'keterangan' => ''
        ];
    }
}
