<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitOrganisasi extends Model
{
    protected $table = 'unit_organisasi';
    protected $fillable = [
        'nama_unit_organisasi',
        'keterangan',
        'kapasitas',
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'nama_unit_organisasi' => '',
            'keterangan' => '',
            'kapasitas' => '',
        ];
    }
}
