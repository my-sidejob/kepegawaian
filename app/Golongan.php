<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Golongan extends Model
{
    protected $table = 'golongan';
    protected $fillable = [
        'pegawai_id',
        'golongan',
        'no_sk',
        'tanggal_sk',
        'tmt_golongan',

    ];

    public static function getDefaultValues()
    {
        return (object) [
            'pegawai_id' => '',
            'golongan' => '',
            'no_sk' => '',
            'tanggal_sk' => '',
            'tmt_golongan' => '',
        ];
    }

    public static function getGolonganList()
    {
        return [
            'I A', 'I B', 'I C',
            'II A', 'II B', 'II C',
            'III A', 'III B', 'III C',
            'IV A', 'IV B', 'IV C'
        ];
    }

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai');
    }
}
