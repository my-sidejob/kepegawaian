@extends('layouts.app')

@section('title', 'Pegawai')



@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Pegawai</h4>
            </div>
            <div class="card-body">
                @if(Auth::user()->level != 'Kepala Sub-bagian')
                    <a href="{{ route('pegawai.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah</a>
                @endif
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pegawai</th>
                            <th>NIK</th>
                            <th>NIP / No. Urut Umum</th>
                            <th>TTL</th>
                            <th>JK</th>
                            <th>No. Telp</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($pegawai as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->namaGelar() }}</td>
                                <td>{{ $row->nik }}</td>
                                <td>{{ $row->jenis_pegawai == 1 ? $row->nik : $row->no_urut_umum }}</td>
                                <td>{{ $row->tempat_lahir . ', '. $row->tanggal_lahir }}</td>
                                <td>{{ $row->jenis_kelamin }}</td>
                                <td>{{ $row->no_telp }}</td>
                                <td>
                                    <span class="badge badge-{{ $row->jenis_pegawai == 1 ? 'info' : 'warning' }}">
                                        {{ $row->jenisPegawai() }}
                                    </span>
                                </td>
                                <td>
                                    <form action="{{ route('pegawai.destroy', $row->id) }}" method="post">
                                        <ul class="d-flex action-button">
                                            <li><a href="{{ route('print.pegawai', $row->id) }}" class="text-info" title="Print Data Pegawai" target="_blank"><i class="fa fa-print"></i></a></li>
                                            @if(Auth::user()->level != 'Kepala Sub-bagian')
                                                <li><a href="{{ route('pegawai.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                                @csrf
                                                @method('delete')
                                                <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li>
                                            @endif
                                        </ul>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="8">Belum ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection

@push('scripts')

<script>
    $('.pegawai-sidebar').addClass('active');
</script>

@endpush
