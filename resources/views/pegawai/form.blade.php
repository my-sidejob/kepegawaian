@extends('layouts.app')

@section('title', 'Form Pegawai')



@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Pegawai</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($pegawai->id)) ? route('pegawai.store') : route('pegawai.update', $pegawai->id) }}" method="post">
                    @csrf
                    @isset($pegawai->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Jenis Pegawai</label>
                                <select name="jenis_pegawai" class="form-control jenis-pegawai" id="jenis-pegawai">
                                    <option value="" class="text-dark"> - Pilih Jenis Pegawai - </option>
                                    <option class="text-dark" value="1" {{ old('jenis_pegawai', $pegawai->jenis_pegawai) == 1 ? 'selected' : '' }}>PNS</option>
                                    <option class="text-dark" value="2" {{ old('jenis_pegawai', $pegawai->jenis_pegawai) == 2 ? 'selected' : '' }}>Non-PNS / Kontrak / Lainnya</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="block-default">
                        <div class="form-group">
                            <label>Nama Pegawai</label>
                            <input type="text" placeholder="Nama pegawai" class="form-control" name="nama" value="{{ old('nama', $pegawai->nama) }}">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Gelar Depan</label>
                                    <input type="text" placeholder="Masukan Gelar Depan" class="form-control" name="gelar_depan" value="{{ old('gelar_depan', $pegawai->gelar_depan) }}">
                                </div>
                                <div class="col-md-6">
                                    <label>Gelar Belakang</label>
                                    <input type="text" placeholder="Masukan Gelar Belakang" class="form-control" name="gelar_belakang" value="{{ old('gelar_belakang', $pegawai->gelar_belakang) }}">
                                </div>
                            </div>
                        </div>
                        <div class="block-default">
                            <div class="form-group">
                                <label>NIK (KTP)</label>
                                <input type="text" placeholder="Masukan NIK" class="form-control" name="nik" value="{{ old('nik', $pegawai->nik) }}">
                            </div>
                        </div>
                        <div class="block-pns" style="display: none">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">NIP</label>
                                        <input type="text" name="nip" placeholder="Masukan NIP"  class="form-control" value="{{ old('nip', $pegawai->nip) }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-non-pns" style="display: none">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>No. Urut Umum</label>
                                        <input type="text" name="no_urut_umum" placeholder="Masukan no urut umum" class="form-control" value="{{ old('no_urut_umum', $pegawai->no_urut_umum) }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Tempat Lahir</label>
                                    <input type="text" placeholder="Masukan Tempat Lahir" class="form-control" name="tempat_lahir" value="{{ old('tempat_lahir', $pegawai->tempat_lahir) }}">
                                </div>
                                <div class="col-md-6">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" placeholder="Masukan Tanggal Lahir" class="form-control datepicker" name="tanggal_lahir" value="{{ old('tanggal_lahir', $pegawai->tanggal_lahir) }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Jenis Kelamin</label>
                                    <select name="jenis_kelamin" class="form-control">
                                        <option value="" class="text-dark"> - Pilih Jenis Kelamin - </option>
                                        <option class="text-dark" value="Laki - laki" {{ old('jenis_kelamin', $pegawai->jenis_kelamin) == 'Laki - laki' ? 'selected' : '' }}>Laki - laki</option>
                                        <option class="text-dark" value="Perempuan" {{ old('jenis_kelamin', $pegawai->jenis_kelamin) == 'Perempuan' ? 'selected' : '' }}>Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>No. Telp</label>
                                    <input type="text" placeholder="Masukan No. Telp" class="form-control" name="no_telp" value="{{ old('no_telp', $pegawai->no_telp) }}">
                                </div>
                                <div class="col-md-6">
                                    <label>Email</label>
                                    <input type="text" placeholder="Masukan Email" class="form-control" name="email" value="{{ old('email', $pegawai->email) }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" class="form-control" placeholder="Masukan Alamat">{{ old('alamat', $pegawai->alamat) }}</textarea>
                        </div>
                    </div>
                    <div class="block-pns" style="display: none">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>SK. PNS</label>
                                    <input type="text" name="sk_pns" class="form-control" value="{{ old('sk_pns', $pegawai->sk_pns) }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="block-pns" style="display: none">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">Terhitung Mulai Tanggal PNS</label>
                                    <input type="text" name="tmt_pns" class="form-control datepicker2" value="{{ old('tmt_pns', $pegawai->tmt_pns) }}">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Terhitung Mulai Tanggal CPNS</label>
                                    <input type="text" name="tmt_cpns" class="form-control datepicker2" value="{{ old('tmt_cpns', $pegawai->tmt_cpns) }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-non-pns" style="display: none">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>No. SK. Kepala Dinas</label>
                                    <input type="text" name="no_sk_kepala_dinas" class="form-control" value="{{ old('no_sk_kepala_dinas', $pegawai->no_sk_kepala_dinas) }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

<script>
    $('.pegawai-sidebar').addClass('active');
    //console.log($('.jenis-pegawai').val());
    if($('.jenis-pegawai').val() == 1) {
        $('.block-pns').show();
        $('.block-non-pns').hide();
        $('.block-default').show();
    }
    if($('.jenis-pegawai').val() == 2) {
        $('.block-pns').hide();
        $('.block-non-pns').show();
        $('.block-default').show();
    }
    $('.jenis-pegawai').change(function(){

        if($(this).val().length == 0){
            $('.block-pns').hide();
            $('.block-default').hide();
            $('.block-non-pns').hide();
        }
        if($(this).val() == 1){
            $('.block-pns').show();
            $('.block-default').show();
            $('.block-non-pns').hide();
        }
        if($(this).val() == 2){
            $('.block-pns').hide();
            $('.block-default').show();
            $('.block-non-pns').show();
        }
    });
</script>

@endpush
