@extends('layouts.app')

@section('title', 'Info Unit Organisasi')



@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Info Unit Organisasi</h4>
            </div>
            <div class="card-body">

                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Unit Organisasi / Bidang</th>
                            <th>Kapasitas</th>
                            <th>Jumlah Skrg</th>
                            <th>Status</th>

                        </tr>
                    </thead>
                    <tbody>
                        @forelse($jabatan_pegawai as $row)
                            @if($row->unitOrganisasi->nama_unit_organisasi == 'Pensiun')
                                @continue
                            @endif
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->unitOrganisasi->nama_unit_organisasi }}</td>
                                <td>{{ $row->unitOrganisasi->kapasitas }}</td>
                                <td>{{ $row->jumlah_sekarang }}</td>
                                <td>
                                    @if($row->unitOrganisasi->kapasitas > $row->jumlah_sekarang)
                                        <span class="badge badge-danger">Kekurangan</span>
                                    @elseif($row->unitOrganisasi->kapasitas == $row->jumlah_sekarang)
                                        <span class="badge badge-success">Jumlah sesuai</span>
                                    @else
                                        <span class="badge badge-warning">Kelebihan</span>
                                    @endif

                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="8">Belum ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection

@push('scripts')

<script>
    $('.pegawai-sidebar').removeClass('active');
</script>

@endpush
