@extends('layouts.app')

@section('title', 'Data Pensiun')


@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>Data Pensiun</h3>
            </div>
            <div class="card-body">
                @if(Auth::user()->level != 'Kepala Sub-bagian')
                    <a href="{{ route('pensiun.create') }}" class="btn btn-primary mb-3">Tambah</a>
                @endif
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Status</th>
                            <th>TMT Pensiun</th>
                            <th>SK Pensiun</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($pensiun->count() != 0)
                            @foreach($pensiun as $row)
                                <tr>
                                    <td>1</td>
                                    <td>{{ $row->pegawai->nik . ' - ' . $row->pegawai->nama }}</td>
                                    <td>{{ $row->unitOrganisasi->nama_unit_organisasi }}</td>
                                    <td>{{ $row->tmt_jabatan }}</td>
                                    <td>{{ $row->no_sk }}</td>
                                    <td>
                                        @if(Auth::user()->level != 'Kepala Sub-bagian')
                                        <form action="{{ route('pensiun.destroy', $row->id) }}" method="post">
                                            <ul class="d-flex action-button">
                                                <li><a href="{{ route('pensiun.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                                @csrf
                                                @method('delete')
                                                <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li>
                                            </ul>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="6">Belum ada data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
</div>

@endsection