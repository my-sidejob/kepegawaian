@extends('layouts.app')

@section('title', 'Form Pensiun')



@section('content')
<div class="row ">  
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Pensiunan</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($pensiun->id)) ? route('pensiun.store') : route('pensiun.update', $pensiun->id) }}" method="post">
                    @csrf
                    @isset($pensiun->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label for="Pegawai">Pegawai</label>
 
                        <select name="pegawai_id" id="" class="mySelect form-control" required>
                            <option value="">- Pilih Pegawai -</option>
                            @foreach($pegawai as $option)
                                <option {{ in_array($option->id, $pensiun_ids) ? 'disabled' : '' }} value="{{ $option->id }}" {{ $option->id == old('pegawai_id', $pensiun->pegawai_id) ? 'selected' : '' }}>{{ $option->nik . ' - ' . $option->nama }}</option>
                            @endforeach
                        </select>

                    </div>
                    <input type="hidden" name="unit_organisasi_id" value="{{ $unit_organisasi_id }}">
                    <input type="hidden" name="jabatan_id" value="{{ $jabatan_id }}">
                    <div class="form-group">
                        <label>TMT Pensiun</label>
                        <input type="text" placeholder="TMT Pensiun" class="form-control datepicker2" name="tmt_jabatan" value="{{ old('tmt_jabatan', $pensiun->tmt_jabatan) }}" required>
                    </div>
                    <div class="form-group">
                        <label>SK Pensiun</label>
                        <input type="text" placeholder="No SK Pensiun" class="form-control" name="no_sk" value="{{ old('no_sk', $pensiun->no_sk) }}" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

