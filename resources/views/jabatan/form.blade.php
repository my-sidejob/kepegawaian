@extends('layouts.app')

@section('title', 'Jabatan')



@section('content')
<div class="row ">  
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Jabatan</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($jabatan->id)) ? route('jabatan.store') : route('jabatan.update', $jabatan->id) }}" method="post">
                    @csrf
                    @isset($jabatan->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">       
                        <label>Nama Jabatan</label>
                        <input type="text" placeholder="Nama Jabatan" class="form-control" name="nama_jabatan" value="{{ old('nama_jabatan', $jabatan->nama_jabatan) }}">
                    </div>
                    <div class="form-group">       
                        <label>Keterangan</label>
                        <textarea name="keterangan" class="form-control" placeholder="Masukan Keterangan (opsional)">{{ old('keterangan', $jabatan->keterangan) }}</textarea>
                    </div>    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>    
            </div>
        </div>
    </div>
</div>
@endsection
