<header class="header my-header">
    <nav class="navbar bg-primary">
        <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">          
                <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn" style="background:#1f9351"><i class="icon-bars"> </i></a><a href="index.html" class="navbar-brand">
                    <div class="brand-text d-none d-md-inline-block"><span> Sistem Informasi Kepegawaian Pada Dinas Pertanian Kota Denpasar </span></div></a>
                </div>
                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                    <!-- Log out-->
                    <form action="{{ route('logout') }}" method="post">
                        @csrf
                        <li class="nav-item"><button type="submit" class="nav-link logout btn text-light"> <span class="d-none d-sm-inline-block"><i class="fa fa-sign-out-alt"></i> Logout</span></button></li>
                    </form>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- #33b35a #1f9351 -->