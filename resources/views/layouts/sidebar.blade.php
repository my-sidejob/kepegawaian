<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                <img src="{{ asset('public/img/icon.jpg') }}" alt="person" class="img-fluid rounded-circle">
                <h2 class="h5">{{ Auth::user()->username }}</h2><span>{{ Auth::user()->level }}</span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo"><a href="/" class="brand-small text-center"> <strong>S</strong><strong class="text-primary">I</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->


        <div class="admin-menu mt-4">
            <h5 class="sidenav-heading">Menu</h5>
            <ul id="side-admin-menu" class="side-menu list-unstyled">
                <li class="{{ (urlHasPrefix('dashboard') == true ) ? 'active' : '' }}"><a href="{{ url('dashboard') }}"> <i class="icon-home"></i>Dashboard</a></li>
                <li class="{{ (urlHasPrefix('unit-organisasi') == true ) ? 'active' : '' }}"><a href="{{ url('unit-organisasi') }}"> <i class="fa fa-globe"></i>Bidang/Unit</a></li>
                <li class="{{ (urlHasPrefix('jabatan') == true ) ? 'active' : '' }}"><a href="{{ url('jabatan') }}"> <i class="fa fa-book-medical"></i>Jabatan</a></li>
                <li class="pegawai-sidebar"><a href="{{ url('pegawai') }}"> <i class="fa fa-user-tie"></i>Pegawai</a></li>
                <li class="{{ (urlHasPrefix('golongan') == true ) ? 'active' : '' }}"><a href="{{ url('golongan') }}"> <i class="fa fa-handshake"></i>Golongan</a></li>
                <li class="{{ (urlHasPrefix('pensiun') == true ) ? 'active' : '' }}"><a href="{{ url('pensiun') }}"> <i class="fa fa-chair"></i>Pensiun</a></li>
                @if(Auth::user()->level == 'Kepala Sub-bagian')
                    <li class="{{ (urlHasPrefix('user') == true ) ? 'active' : '' }}"><a href="{{ url('user') }}"> <i class="fa fa-user"></i>User</a></li>
                @endif
                <li class="{{ (urlHasPrefix('mutasi') == true ) ? 'active' : '' }}"><a href="{{ url('mutasi') }}"> <i class="fa fa-recycle"></i>Mutasi</a></li>
                @if(Auth::user()->level == 'Kepala Sub-bagian')
                <li class="{{ (urlHasPrefix('ekse-pegawai') == true ) ? 'active' : '' }}"><a href="{{ url('info-unit-organisasi') }}"> <i class="fa fa-chart-bar"></i>Info Unit Organisasi</a></li>
                @endif
            </ul>
        </div>

    </div>
</nav>
