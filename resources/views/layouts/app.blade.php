<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title') - Sistem Kepegawaian</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('public/templates/vendor/bootstrap/css/bootstrap.min.css') }} ">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('public/templates/vendor/font-awesome/css/all.min.css') }}">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{ asset('public/templates/css/fontastic.css') }}">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="{{ asset('public/templates/css/grasp_mobile_progress_circle-1.0.0.min.css') }}">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="{{ asset('public/templates/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('public/templates/css/style.default.css') }}" id="theme-stylesheet">
    <!-- Data tables-->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('public/css/customize.css') }}">
    <!-- Select2js -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <!-- datepicker -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="icon" href="{{ asset('public/img/icon.png') }}" type="image/gif" sizes="16x16">
    <style>
      .block-default {
        display: none;
      }
    </style>
  </head>
  <body>
    
  <!-- sidebar -->
  @auth
    @include('layouts.sidebar')
      <div class="page">
          <!-- navbar-->
          @include('layouts.header')
          <!-- Breadcrumb-->
          
          <div class="breadcrumb-holder mb-5">
            <div class="container-fluid">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">@yield('title')</a></li>
                               
                </ul>
            </div>
        </div>
        <section class="forms">
            <div class="container-fluid">
                <!-- Page Header-->
                @include('layouts.flash')

                
                @yield('content')
            </div>
        </section>
      
    @include('layouts.footer')
    
    </div>
  @endauth


@guest()
   @yield('content')
@endguest
    <!-- JavaScript files-->
    <script src="{{ asset('public/templates/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('public/templates/vendor/popper.js/umd/popper.min.js') }}"> </script>
    <script src="{{ asset('public/templates/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('public/templates/js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
    <script src="{{ asset('public/templates/vendor/jquery.cookie/jquery.cookie.js') }}"> </script>
    <script src="{{ asset('public/templates/vendor/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('public/templates/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <!-- Main File-->
    <script src="{{ asset('public/templates/js/front.js') }}"></script>
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('public/templates/vendor/cleave.js/dist/cleave.min.js') }}"></script>

    <script>
      $(document).ready( function () {
        //data table
        $('.datatable').DataTable({
          "pageLength": 50
        });
        
        //select2 input option
        $('.mySelect').select2();

        //datepicker      
        $( ".datepicker" ).datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: '1900:',
          dateFormat: 'yy-mm-dd',
          maxDate: 0,
          defaultDate: '1990-01-01'
        });

        $( ".datepicker2" ).datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: '1900:',
          dateFormat: 'yy-mm-dd',
          maxDate: 0,
        });
  

      });


      //format duit sudah ga dipake
      // $(document).ready(function(){
      //   //cleave (for currency)
      //   var cleave = new Cleave('.thousand-input', {
      //     numeral: true,
      //     numeralThousandsGroupStyle: 'thousand'
      //   });
      // });
      
    </script>

    @stack('scripts')
  </body>
</html>