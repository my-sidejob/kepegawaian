@extends('layouts.app')

@section('title', 'Golongan')



@section('content')
<div class="row ">
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Golongan</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($golongan->id)) ? route('golongan.store') : route('golongan.update', $golongan->id) }}" method="post">
                    @csrf
                    @isset($golongan->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Pegawai</label>
                        <select name="pegawai_id" id="" class="mySelect form-control">
                            <option value="">- Pilih Pegawai -</option>
                            @foreach($pegawai as $option)
                                <option value="{{ $option->id }}" {{ $option->id == old('pegawai_id', $golongan->pegawai_id) ? 'selected' : '' }}>{{ $option->nik . ' - ' . $option->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Golongan</label>
                                <select name="golongan" id="" class="mySelect form-control">
                                    <option value="">- Pilih Golongan -</option>
                                    @foreach(App\Golongan::getGolonganList() as $option)
                                        <option value="{{ $option }}" {{ $option == old('golongan', $golongan->golongan) ? 'selected' : '' }}>{{ $option }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="">No. SK</label>
                                <input type="text" name="no_sk" value="{{ old('no_sk', $golongan->no_sk) }}" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Tanggal SK</label>
                                <input type="text" name="tanggal_sk" value="{{ old('tanggal_sk', $golongan->tanggal_sk) }}" class="form-control datepicker2">
                            </div>
                            <div class="col-md-6">
                                <label for="">TMT Golongan</label>
                                <input type="text" name="tmt_golongan" value="{{ old('tmt_golongan', $golongan->tmt_golongan) }}" class="form-control datepicker2">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-primary float-right mt-4" value="Simpan">
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
@endsection
