@extends('layouts.app')

@section('title', 'Mutasi')



@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Pegawai</h4>
            </div>
            <div class="card-body">
                <form action="" method="get">
                    <div class="form-group">
                        <label for="Pegawai">Pegawai</label>
                        <select name="pegawai_id" id="" class="mySelect form-control">
                            <option value="">- Pilih Pegawai -</option>
                            @foreach($pegawai as $option)
                                <option value="{{ $option->id }}" {{ $option->id == old('pegawai_id', $_GET['pegawai_id'] ?? '') ? 'selected' : '' }}>{{ $option->nik . ' - ' . $option->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Cari" class="btn btn-primary float-right">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@if(isset($pegawai_single))

<div class="row">
    <div class="col-md-12">
        @if(Auth::user()->level == 'Kepala Sub-bagian')
            @if($jabatan_pegawai->count() != 0)
                @if($jabatan_pegawai[0]->unitOrganisasi->nama_unit_organisasi != 'Pensiun')
                    <a href="{{ route('mutasi.create') }}?pegawai_id={{ $_GET['pegawai_id'] }}" class="btn btn-primary mb-3">Tambah Mutasi</a>
                @endif
            @else
                <a href="{{ route('mutasi.create') }}?pegawai_id={{ $_GET['pegawai_id'] }}" class="btn btn-primary mb-3">Tambah Mutasi</a>
            @endif
        @endif
        <a href="{{ route('print.mutasi', $_GET['pegawai_id']) }}" class="btn btn-outline-primary mb-3 ">Print Mutasi</a>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3>Jabatan Sekarang</h3>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Unit Organisasi</th>
                            <th>Jabatan</th>
                            <th>Eselon</th>
                            <th>TMT Jabatan</th>
                            <th>Tgl SK</th>
                            <th>No SK</th>
                            <th>Print</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($jabatan_pegawai->count() != 0)
                        <tr>
                            <td>1</td>
                            <td>{{ $jabatan_pegawai[0]->pegawai->nik . ' - ' . $jabatan_pegawai[0]->pegawai->nama }}</td>
                            <td>{{ $jabatan_pegawai[0]->unitOrganisasi->nama_unit_organisasi }}</td>
                            <td>{{ $jabatan_pegawai[0]->jabatan->nama_jabatan }}</td>
                            <td>{{ $jabatan_pegawai[0]->eselon }}</td>
                            <td>{{ $jabatan_pegawai[0]->tmt_jabatan }}</td>
                            <td>{{ $jabatan_pegawai[0]->tgl_sk }}</td>
                            <td>{{ $jabatan_pegawai[0]->no_sk }}</td>
                            <td>
                                @if($jabatan_pegawai[0]->unitOrganisasi->nama_unit_organisasi != 'Pensiun')
                                    <a href="{{ route('print.suratPenugasan', $jabatan_pegawai[0]->id) }}" target="_blank">Surat Penugasan</a>
                                @endif
                            </td>
                        </tr>
                        @else
                            <tr>
                                <td colspan="7">Belum ada data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3>Riwayat Mutasi</h3>

                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Unit Organisasi</th>
                            <th>Jabatan</th>
                            <th>Eselon</th>
                            <th>TMT Jabatan</th>
                            <th>Tgl SK</th>
                            <th>No SK</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($jabatan_pegawai->count() >= 2)
                            @foreach($jabatan_pegawai as $key => $row)
                                @if($key == 0)
                                    @continue
                                @endif
                                <tr>
                                    <td>{{ $loop->iteration - 1 }}</td>
                                    <td>{{ $row->pegawai->nik . ' - ' . $row->pegawai->nama }}</td>
                                    <td>{{ $row->unitOrganisasi->nama_unit_organisasi }}</td>
                                    <td>{{ $row->jabatan->nama_jabatan }}</td>
                                    <td>{{ $row->eselon }}</td>
                                    <td>{{ $row->tmt_jabatan }}</td>
                                    <td>{{ $row->tgl_sk }}</td>
                                    <td>{{ $row->no_sk }}</td>
                                    <td>
                                        @if(Auth::user()->level == 'Kepala Sub-bagian')
                                            <form action="{{ route('mutasi.destroy', $row->id) }}?pegawai_id={{ $_GET['pegawai_id'] }}" method="post">
                                                <ul class="d-flex action-button">
                                                    <li><a href="{{ route('mutasi.edit', $row->id) }}?pegawai_id={{ $_GET['pegawai_id'] }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                                    @csrf
                                                    @method('delete')
                                                    <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li>
                                                </ul>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">Belum ada data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endif



@endsection

@push('scripts')

<script>
    //$('.pegawai-sidebar').addClass('active');
</script>

@endpush
