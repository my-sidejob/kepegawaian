@extends('layouts.app')

@section('title', 'Form Mutasi')



@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Mutasi</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($mutasi->id)) ? route('mutasi.store') : route('mutasi.update', $mutasi->id) }}" method="post">
                    @csrf
                    @isset($mutasi->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label for="Pegawai">Pegawai</label>

                        <select name="" id="" class="mySelect form-control" disabled>
                            <option value="">- Pilih Pegawai -</option>
                            @foreach($pegawai as $option)
                                <option value="{{ $option->id }}" {{ $option->id == old('pegawai_id', $_GET['pegawai_id']) ? 'selected' : '' }}>{{ $option->nik . ' - ' . $option->nama }}</option>
                            @endforeach
                        </select>

                        <input type="hidden" name="pegawai_id" value="{{ old('pegawai_id', $_GET['pegawai_id']) }}">
                    </div>
                    <div class="form-group">
                        <label>Unit Organisasi</label>
                        <select name="unit_organisasi_id" id="" class="mySelect form-control">
                            <option value="">- Pilih Unit Organisasi -</option>
                            @foreach($unit_organisasi as $option)
                                <option value="{{ $option->id }}" {{ $option->id == old('unit_organisasi_id', $mutasi->unit_organisasi_id) ? 'selected' : '' }}>{{ $option->nama_unit_organisasi }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Jabatan</label>
                        <select name="jabatan_id" id="" class="mySelect form-control">
                            <option value="">- Pilih Jabatan -</option>
                            @foreach($jabatan as $option)
                                <option value="{{ $option->id }}" {{ $option->id == old('jabatan_id', $mutasi->jabatan_id) ? 'selected' : '' }}>{{ $option->nama_jabatan }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Eselon</label>
                        <select name="eselon" id="" class="mySelect form-control">
                            <option value="">- Pilih Eselon -</option>
                            @foreach(App\JabatanPegawai::eselonList() as $option)
                                <option value="{{ $option }}" {{ $option == old('eselon', $mutasi->eselon) ? 'selected' : '' }}>{{ $option }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>TMT Jabatan</label>
                        <input type="text" placeholder="TMT Jabatan" class="form-control datepicker2" name="tmt_jabatan" value="{{ old('tmt_jabatan', $mutasi->tmt_jabatan) }}">
                    </div>
                    <div class="form-group">
                        <label>Tanggal SK</label>
                        <input type="text" placeholder="Tanggal SK" class="form-control datepicker2" name="tgl_sk" value="{{ old('tgl_sk', $mutasi->tgl_sk) }}">
                    </div>
                    <div class="form-group">
                        <label>No. SK</label>
                        <input type="text" placeholder="No. SK" class="form-control" name="no_sk" value="{{ old('tgl_sk', $mutasi->no_sk) }}">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
