@extends('layouts.app')

@section('title', 'Form User')



@section('content')
<div class="row ">  
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah User</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($user->id)) ? route('user.store') : route('user.update', $user->id) }}" method="post">
                    @csrf
                    @isset($user->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" placeholder="Username" class="form-control" name="username" value="{{ old('username', $user->username) }}">
                    </div>
                    <div class="form-group">       
                        <label>Password</label>
                        <input type="password" placeholder="Password" class="form-control" name="password" value="">
                    </div>
                    <div class="form-group">
                        <label>Level</label>
                        <select name="level" class="form-control jenis-pegawai" id="jenis-pegawai">
                            <option value="" class="text-dark"> - Pilih Level - </option>
                            <option class="text-dark" value="Kepala Sub-bagian" {{ old('level', $user->level) == "Kepala Sub-bagian" ? 'selected' : '' }}>Kepala Sub-bagian</option>
                            <option class="text-dark" value="Pegawai Sub-bagian" {{ old('level', $user->level) == "Pegawai Sub-bagian" ? 'selected' : '' }}>Pegawai Sub-bagian</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
