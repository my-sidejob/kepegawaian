@extends('layouts.app')

@section('title', 'Pegawai Perbidang')



@section('content')
<div class="row ">  
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Pegawai Perbidang</h4>
            </div>
            <div class="card-body">
                <a href="{{ route('print.pegawaiOrganisasi', $unit_organisasi->id) }}" class="btn btn-primary mb-3" target="_blank">Print</a>
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Unit Organisasi</th>
                            <th>Jabatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($pegawai->count() != 0)
                            @foreach($pegawai as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->pegawai->nik }}</td> 
                                    <td>{{ $row->pegawai->nama }}</td>
                                    <td>{{ $unit_organisasi->nama_unit_organisasi }}</td>
                                    <td>{{ $row->jabatan->nama_jabatan }}</td>
                                </tr>   
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="3">Belum ada data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
