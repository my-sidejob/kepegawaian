@extends('layouts.app')

@section('title', 'Unit Organisasi')



@section('content')
<div class="row ">  
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Bidang</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($unit_organisasi->id)) ? route('unit-organisasi.store') : route('unit-organisasi.update', $unit_organisasi->id) }}" method="post">
                    @csrf
                    @isset($unit_organisasi->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">       
                        <label>Nama</label>
                        <input type="text" placeholder="Nama Bidang/Unit" class="form-control" name="nama_unit_organisasi" value="{{ old('nama_unit_organisasi', $unit_organisasi->nama_unit_organisasi) }}">
                    </div>
                    <div class="form-group">
                        <label>Kapasitas</label>
                        <input type="number" placeholder="Kapasitas Pegawai" class="form-control" name="kapasitas" value="{{ old('kapasitas', $unit_organisasi->kapasitas) }}">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="keterangan" class="form-control" placeholder="Masukan Keterangan (opsional)">{{ old('keterangan', $unit_organisasi->keterangan) }}</textarea>
                    </div>    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>    
            </div>
        </div>
    </div>
</div>
@endsection
