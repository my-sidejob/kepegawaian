@extends('layouts.app')

@section('title', 'Bidang/Unit')



@section('content')
<div class="row ">  
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Bidang/Unit</h4>
            </div>
            <div class="card-body">
                @if(Auth::user()->level != 'Kepala Sub-bagian')
                    <a href="{{ route('unit-organisasi.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah</a>
                @endif
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Bidang/Unit</th>
                            <th>Kapasitas</th>
                            <th>Keterangan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($unit_organisasi->count() != 0)
                            @foreach($unit_organisasi as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->nama_unit_organisasi }}</td> 
                                    <td>{{ $row->kapasitas }}</td>
                                    <td>{{ $row->keterangan }}</td>
                                    <td>
                                        
                                        <form action="{{ route('unit-organisasi.destroy', $row->id) }}" method="post">
                                            <ul class="d-flex action-button">                                        
                                                <li><a href="{{ route('unit-organisasi.show', $row->id) }}" class="text-primary" title="Lihat Pegawai"><i class="fa fa-user-tie"></i></a></li>                                                
                                                @if(Auth::user()->level != 'Kepala Sub-bagian')
                                                    <li><a href="{{ route('unit-organisasi.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                                    @csrf
                                                    @method('delete')
                                                    <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li>
                                                @endif
                                            </ul>
                                        </form>
                                        
                                    </td>
                                </tr>   
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="3">Belum ada data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
