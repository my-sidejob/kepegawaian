<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Unit Organisasi</title>
    <style>
        * {
            font-family: "Arial"
        }

        .print-table {
            width: 100%;
            border: 1px solid #222;
            border-bottom: 0;
        }

        .print-table td, .print-table th {
            border-bottom: 1px solid #222;
            padding:4px 0;
        }

        .print-table th{
            text-align: left;
        }
    </style>
</head>
<body>
    <div class="header">
        <img src="{{ public_path('img/kop.png') }}" alt="kop" width="100%">
        
    </div>
    
    <h3>Data Pegawai Perbidang</h3>

    <table class="print-table">
        <thead>
            <tr>
                <th>No</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>Unit Organisasi</th>
                <th>Jabatan</th>
            </tr>
        </thead>
        <tbody>
            @if($pegawai->count() != 0)
                @foreach($pegawai as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->pegawai->nik }}</td> 
                        <td>{{ $row->pegawai->nama }}</td>
                        <td>{{ $unit_organisasi->nama_unit_organisasi }}</td>
                        <td>{{ $row->jabatan->nama_jabatan }}</td>
                    </tr>   
                @endforeach
            @else 
                <tr>
                    <td colspan="3">Belum ada data</td>
                </tr>
            @endif
        </tbody>
    </table>
</body>
</html>