<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Unit Organisasi</title>
    <style>
        * {
            font-family: "Arial"
        }

        .print-table {
            width: 100%;
            border: 1px solid #222;
            border-bottom: 0;
        }

        .print-table td, .print-table th {
            border-bottom: 1px solid #222;
            padding:4px 0;
        }

        .print-table th{
            text-align: left;
        }
    </style>
</head>
<body>
    <div class="header">
        <img src="{{ public_path('img/kop.png') }}" alt="kop" width="100%">

    </div>

    <h3>Jabatan Sekarang</h3>

    <table class="print-table">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Unit Organisasi</th>
                <th>Jabatan</th>
                <th>Eselon</th>
                <th>TMT Jabatan</th>
                <th>Tgl SK</th>
                <th>No SK</th>
            </tr>
        </thead>
        <tbody>
            @if($jabatan_pegawai->count() != 0)
            <tr>
                <td>1</td>
                <td>{{ $jabatan_pegawai[0]->pegawai->nik . ' - ' . $jabatan_pegawai[0]->pegawai->nama }}</td>
                <td>{{ $jabatan_pegawai[0]->unitOrganisasi->nama_unit_organisasi }}</td>
                <td>{{ $jabatan_pegawai[0]->jabatan->nama_jabatan }}</td>
                <th>{{ $jabatan_pegawai[0]->eselon }}</th>
                <td>{{ $jabatan_pegawai[0]->tmt_jabatan }}</td>
                <td>{{ $jabatan_pegawai[0]->tgl_sk }}</td>
                <td>{{ $jabatan_pegawai[0]->no_sk }}</td>
            </tr>
            @else
                <tr>
                    <td colspan="7">Belum ada data</td>
                </tr>
            @endif
        </tbody>
    </table>

    <h3>Riwayat Mutasi Pegawai</h3>
    <table class="print-table">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Unit Organisasi</th>
                <th>Jabatan</th>
                <th>Eselon</th>
                <th>TMT Jabatan</th>
                <th>Tgl SK</th>
                <th>No SK</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @if($jabatan_pegawai->count() >= 2)
                @foreach($jabatan_pegawai as $key => $row)
                    @if($key == 0)
                        @continue
                    @endif
                    <tr>
                        <td>{{ $loop->iteration - 1 }}</td>
                        <td>{{ $row->pegawai->nik . ' - ' . $row->pegawai->nama }}</td>
                        <td>{{ $row->unitOrganisasi->nama_unit_organisasi }}</td>
                        <td>{{ $row->jabatan->nama_jabatan }}</td>
                        <td>{{ $row->eselon }}</td>
                        <td>{{ $row->tmt_jabatan }}</td>
                        <td>{{ $row->tgl_sk }}</td>
                        <td>{{ $row->no_sk }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="7">Belum ada data</td>
                </tr>
            @endif
        </tbody>
    </table>
</body>
</html>
