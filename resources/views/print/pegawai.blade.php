<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Unit Organisasi</title>
    <style>
        * {
            font-family: "Arial"
        }

        .print-table {
            width: 100%;
            border-bottom: 0;
        }

        .print-table td, .print-table th {
            padding:4px 0;
        }

        .print-table th{
            text-align: left;
        }

        .print-table tr td:nth-child(2) {
            width: 20px;
        }
        .print-table tr td:nth-child(1) {
            width: 170px;
        }
    </style>
</head>
<body>
    <div class="header">
        <img src="{{ public_path('img/kop.png') }}" alt="kop" width="100%">

    </div>
    <h3>Data Pegawai</h3>

    <table class="print-table">
        <tr>
            <td>NIK</td>
            <td>:</td>
            <td>{{ $pegawai->nik }}</td>
        </tr>
        @if($pegawai->jenis_pegawai == 1)
        <tr>
            <td>NIP</td>
            <td>:</td>
            <td>{{ $pegawai->nip }}</td>
        </tr>
        @else
        <tr>
            <td>No. Urut Umum</td>
            <td>:</td>
            <td>{{ $pegawai->no_urut_umum }}</td>
        </tr>
        @endif
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $pegawai->namaGelar() }}</td>
        </tr>
        @if($jabatan_sekarang != null)
            <tr>
                <td>Unit Organisasi</td>
                <td>:</td>
                <td>{{ $jabatan_sekarang->unitOrganisasi->nama_unit_organisasi }}</td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td>{{ $jabatan_sekarang->jabatan->nama_jabatan }}</td>
            </tr>
        @endif
        <tr>
            <td>Tempat, Tanggal Lahir</td>
            <td>:</td>
            <td>{{ $pegawai->tempat_lahir . ', ' . date("d-m-Y", strtotime($pegawai->tanggal_lahir)) }}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            <td>{{ $pegawai->jenis_kelamin }}</td>
        </tr>
        <tr>
            <td>No. Telp</td>
            <td>:</td>
            <td>{{ $pegawai->no_telp }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td>{{ $pegawai->alamat }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td>{{ $pegawai->email }}</td>
        </tr>
        <tr>
            <td>Jenis Pegawai</td>
            <td>:</td>
            <td>{{ $pegawai->jenisPegawai() }}</td>
        </tr>
        @if($pegawai->jenis_pegawai == 2)
            <tr>
                <td>No SK Kepala Dinas</td>
                <td>:</td>
                <td>{{ $pegawai->no_sk_kepala_dinas }}</td>
            </tr>
        @else
            <tr>
                <td>SK. PNS</td>
                <td>:</td>
                <td>{{ $pegawai->sk_pns }}</td>
            </tr>
            <tr>
                <td>TMT PNS</td>
                <td>:</td>
                <td>{{ date("d-m-Y", strtotime($pegawai->tmt_pns)) }}</td>
            </tr>
            <tr>
                <td>TMT CPNS</td>
                <td>:</td>
                <td>{{ date("d-m-Y", strtotime($pegawai->tmt_cpns)) }}</td>
            </tr>
        @endif
    </table>
</body>
</html>
