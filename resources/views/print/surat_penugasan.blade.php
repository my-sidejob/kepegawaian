<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Unit Organisasi</title>
    <style>
        * {
            font-family: "Arial"
        }

        .print-table {
            width: 100%;
            border-bottom: 0;
        }

        .print-table td, .print-table th {
            padding:4px 0;
        }

        .print-table th{
            text-align: left;
        }

        .print-table tr td:nth-child(2) {
            width: 20px;
        }
        .print-table tr td:nth-child(1) {
            width: 170px;
        }
        .ttd {
            line-height: 1em;
            text-align: right;
        }
    </style>
</head>
<body>
    <div class="header">
        <img src="{{ public_path('img/kop.png') }}" alt="kop" width="100%">
        
    </div>
    <div style="text-align:center; ">
        <h3 style="text-decoration: underline; font-weight:400">SURAT PERINTAH TUGAS</h3>
        <h3 style="font-weight:400">Nomor : {{ $jabatan_pegawai->created_at->format('Y') }} / {{ $jabatan_pegawai->id }}</h3>
    </div>
    <div>
        <p>Yang bertanda tangan dibawah ini:</p>
        <table>
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td>Ir. I  Gede Ambara Putra, M.Agb</td>
            </tr>
            <tr>
                <td>NIP</td>
                <td>:</td>
                <td>19640112 199203 1 011</td>
            </tr>
            <tr>
                <td>Pangkat/Gol</td>
                <td>:</td>
                <td>Pembina Utama Muda  (IV/c)</td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td>Kepala  Dinas Pertanian Kota  Denpasar</td>
            </tr>
        </table>

        <p>Menugaskan kepada:</p>
        <table>
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $jabatan_pegawai->pegawai->nik }}</td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td>{{ $jabatan_pegawai->pegawai->nama }}</td>
            </tr>
            @if($jabatan_pegawai->jenis_pegawai == '1')
                <tr>
                    <td>NIP</td>
                    <td>:</td>
                    <td>19640112 199203 1 011</td>
                </tr>
            @endif
            @if($golongan != null)
                <tr>
                    <td>Pangkat/Gol</td>
                    <td>:</td>
                    <td>{{ $golongan->golongan }}</td>
                </tr>
            @endif
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td>{{ $jabatan_pegawai->jabatan->nama_jabatan }}</td>
            </tr>
            <tr>
                <td>Untuk</td>
                <td>:</td>
                <td>Melaksanakan tugas sebagai {{ $jabatan_pegawai->jabatan->nama_jabatan }} di {{ $jabatan_pegawai->unitOrganisasi->nama_unit_organisasi }}, terhitung mulai tanggal {{ date('d-m-Y', strtotime($jabatan_pegawai->tmt_jabatan)) }}.</td>
            </tr>
        </table>

        <p>Demikian surat perintah tugas ini dibuat untuk dilaksanakan sebagaimana mestinya.</p>
        <div class="ttd">
            <br><br>
            <p>Denpasar, {{ date('d-m-Y', strtotime($jabatan_pegawai->tmt_jabatan)) }}</p>
            <p>Kepala Dinas Pertanian Kota  Denpasar,</p>
            <br><br><br>
            <p><b>Ir. I Gede Ambara Putra,M.Agb</b></p>
            <p>Pembina Utama Muda</p>
            <p>NIP. 196450112 199203 1 011</p>
        </div>
            
    </div>


    
</body>
</html>