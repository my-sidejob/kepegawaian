<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJabatanPegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jabatan_pegawai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('pegawai_id');
            $table->unsignedBigInteger('unit_organisasi_id');
            $table->unsignedBigInteger('jabatan_id');
            $table->date('tmt_jabatan');
            $table->date('tgl_sk');
            $table->string('no_sk');
            $table->string('jabatan_sekarang')->nullable();
            $table->foreign('pegawai_id')->references('id')->on('pegawai');
            $table->foreign('unit_organisasi_id')->references('id')->on('unit_organisasi');         
            $table->foreign('jabatan_id')->references('id')->on('jabatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jabatan_pegawai');
    }
}
