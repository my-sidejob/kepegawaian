<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->string('gelar_depan')->nullable();
            $table->string('gelar_belakang')->nullable();
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->enum('jenis_kelamin', ['Laki - laki', 'Perempuan']);
            $table->string('alamat');
            $table->string('no_telp');
            $table->string('email');
            $table->string('nik')->unique();
            $table->string('nip')->nullable(); //(unique)
            $table->date('tmt_pns')->nullable();
            $table->string('seri_karpeg')->nullable();
            $table->date('tmt_cpns')->nullable();
            $table->string('bidang_ilmu')->nullable();
            $table->string('eselon')->nullable();
            $table->string('jenis_pegawai'); 
            $table->string('no_sk_kepala_dinas')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pegawai', function (Blueprint $table) {
            //
        });
    }
}
